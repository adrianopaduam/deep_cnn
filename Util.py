#Single Image Delinearization (Used in Prediction)
def delinearize(mask, shape):
    import numpy as np
    from cythonyze import delinearize_cython
    
    if len(shape) != 4:
        import sys
        sys.exit("Dataset.Delinearize(): Wrong Shape Format. Try (d, w, h, 1)")
    
    new_mask = delinearize_cython(mask, shape)
        
    return new_mask


#Function for Confusion Matrix plotting
def plotConfMatrix(mtx):
    from matplotlib import pyplot as plt
    cm = mtx.astype('float') / mtx.sum(axis=1)[:, np.newaxis]
    classes = ['CG', 'PG']


    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("Confusion Matrix")
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),horizontalalignment="center", color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
